# Cours Ensicaen 3A
## Introduction à Unity
### TP1: Le prochain triple A


## Introduction 
Dans ce tp, nous allons revoir l'essentiel d'Unity. Pour cela, nous allons réaliser un jeu au visuel très simple avec des mécaniques bien connues et simples à mettre en place. 
Ce jeu sera un simple plateformer avec un environnement en 3D et des mouvements en 2D.
Bien évidemment, vous êtes libres de l'améliorer autant que vous le désirez. Le principal c'est que les bases soient acquises. 

N'hésitez pas à me contacter à tout moment sur l'adresse <kubiak@ecole.ensicaen.fr> si vous êtes bloqués ou que vous avez une quelconque question. 

Notions acquises durant le tp:
- Développement d'un comportement en Utilisant les MonoBehavior
- Séparation du comportement et des données grâce aux ScriptablesObjects
- Changements basiques sur les transform
- Utilisation des évènements Unity pour détecter les collisions
- Prefabs & Instanciation de nouveaux objets
- Changement de jeu de données en fonction de l'état

Vous trouverez tout à la fin mes critères de notation.

## Intialisation du projet
- Commencez par ouvrir Unity Hub et créez un nouveau projet en 3D
- Commencez par créer une arboresence propre dans l'onglet Project:
  - Scripts
  - Resources
    - Prefabs
    - Scenes
    - 3D
      - Models
    - 2D
      - Sprites

## Initialisation de la scène
- Comme sol nous allons utiliser un cube:
    - Dans la hiérarchie, clic droit -> 3D Object -> Cube
    - Dans l'inspector, cliquez sur les trois petis points en haut à droite et faites "Reset"
    - Toujours dans l'Inspector, mettez une valeur plus grande en X ou en Y pour Scale
- Pour replacer la caméra:
    - Positionnez vous correctement dans la Scene View, sélectionnez la caméra puis faites Ctrl/Command + Shift + F (cela va aligner la Camera avec la vue).
- Créer un nouveau un Cube en suivant la même méthode, gardez une taille de 1 et placez le au dessus 

> **Remarques:**
> - Ctrl/Command + Shift + F: change le transform de l'objet sélectionné par celui de la Caméra de la Scene View.

## Mouvement du personnage - 10 Points
Maintenant que notre scène est initialisée, nous allons pouvoir donner vie à notre personnage.
- Sous Scripts, créez un script que vous pouvez appeler PlayerController. Vous y ajouterez la mécanique nécessaire pour qu'il se déplace sur le bon axe. 
- Ensuite ajoutez et configurez ce script sur le joueur.
- Vous en profiterez pour lui ajouter un component RigidBody.

<details>
<summary> Aller plus loin </summary>

- Il est possible d'externaliser la dépendance au code liée aux entrées utilisateurs.
</details>


## Suivi de la Caméra - 5 Points
Maintenant nous allons faire en sorte que la caméra suive les déplacements de notre joueur.
- Sous Scripts, créez un script que vous pouvez appeler CameraController et nous allons suivre la même méthode que pour le Joueur en changeant la position.
- Ensuite proposez une méthode afin que cela se fasse de façon lissée.

<details>
<summary> Aide </summary>

Vous pouvez suivre ce [tutoriel](https://www.youtube.com/watch?v=MFQhpwc6cKE) qui explique très bien comment le faire. N'hésitez pas à me demander s'il y a des incompréhensions.
</details>

## Saut du personnage - 5 Points
- Rajoutez un comportement qui va permettre au joueur de sauter, vous pouvez le faire dans le PlayerController ou créer un nouveau script pour cela.
- Faites en sorte que le joueur ne puisse pas sauter à l'infini.
- Sauvegardez la configuration du personnage dans un ScriptableObject.

<details>
<summary> Aide </summary>

- Il vous suffit de récupérer le RigidBody du joueur avec GetComponent<RigidBody>. Ensuite, si il y a pression sur la bonne touche appelez AddForce sur ce RigidBody.
- Vous pouvez dans la méthode OnCollisionEnter de votre Script vérifier que vous touchez le sol, si c'est le cas vous pouvez sauter. Je vous conseille de mettre un tag sur votre objet sol et utiliser la méthode CompareTag de Unity au moment de la collision.
- Nous avons vu comment créer un ScriptableObject en cours, sinon vous pouvez me demandez ou suivre le [tutoriel](https://www.youtube.com/watch?v=PVOVIxNxxeQ) de Unity.
</details>

<details>
<summary> Aller plus loin </summary>

- Faites en sorte que le joueur puisse faire un double saut
- Rendre le saut multiple configurable si ce n'est pas le cas
</details>

## Rendre votre joueur dangereux - Bonus
Nous allons maintenant faire en sorte que votre joueur puisse tirer:
- Créez un objet dans la scène, il nous servira de balle. Ajoutez lui un script qui le fera avancer en continu. Transformez le en Prefab.
- Créez un script qui va gérez le fait qu'on tir. Il instancie la balle au bon endroit et avec la bonne orientation quand on appuie sur une touche. 
- Ensuite vous prendrez en compte la durée de vie de la balle. Elle devra se supprimer en cas de collision, ou qu'il dépasse une certaine durée de vie.

<details>
    <summary> Aide </summary>

- Vous utiliserez la méthode Instantiate sur un Prefab que vous aurez en référence. 
</details>

<details>
<summary> Aller plus loin </summary>

- Faire plusieurs types d'armes
- Utiliser le principe de pooling en utilisant un Asset: LeanPool.
</details>


## Aller plus loin - Partie Bonus
Pour la partie aller plus loin vous pouvez juste concevoir le système et proposer un architecture, cela sera pris en compte dans la notation, c'est presque plus important que l'implémentation.

- Externalisation des dépendances (me demander si vous en êtes là)

- Animation de plateforme
  - Tentez de rendre votre code le plus modulable et le plus optimisé possible
  - Il est possible de faire des choses très propres et optimisées en combinant Coroutine et AnimationCurve, je vous laisse me demander si ça vous intéresse.

- Ajout d'un système de plaque de pression avec événement
    - Vous ajouterez un objet qui quand on passe dessus déclenche une action
    - Vous pourrez déclencher l'ouverture d'une porte, mais je laisse libre cours à votre imagination

- Rajouter des ennemis:
  - S'ils percoivent le joueur: tirent dessus, suivent le joueur.
  - Prise en compte de la visibilité pour la détection
  - Système d'état avec patrouille, recherche, etc.

- Prise en compte de la mort pour le joueur et les ennemis:
  - Par tir
  - Par chute
  - Eventuellement, vous pouvez prendre en compte l'ajout d'une barre de vie 

- Ajout d'une UI pour le jeu


## Critère de notation
- Qualité du code
- Utilisation de SerializeField plutôt que des membres publiques
- Architecture de qualité
- Rapport clair